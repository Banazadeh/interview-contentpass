import express from 'express';
import {param, body} from 'express-validator';
import validator from 'validator';
import Document, {Paragraph} from './storage';

export const paragraphRouter = express.Router();
// we need to use mergeParams to access params of the parent router
export const sentenceRouter = express.Router({mergeParams: true});

// nesting sentenceRouter in paragraphRouter
paragraphRouter.use('/:slug/sentence', sentenceRouter);

paragraphRouter
    .route('/:slug')
    .get(
        param('slug').isString(),
        (req: express.Request, res: express.Response) => {
          const {params: {slug}} = req;
          let p: Paragraph;

          try {
            p = Document.getParagraph(slug);
          } catch (e) {
            console.log(e);
            res.status(404).send();
            return;
          }

          const {complete, sentences} = p;

          res.status(200).send({
            complete,
            sentences,
          });
        })
    .post(
        param('slug').isString(),
        body('numSentences').isInt({min: 0}),
        (req: express.Request, res: express.Response) => {
          const {params: {slug}, body: {numSentences}} = req;

          try {
            Document.createParagraph(slug, numSentences);
          } catch (e) {
            console.log(e);
            res.status(400).send();
            return;
          }

          res.status(200).send();
        })
    .delete(
        param('slug').isString(),
        (req: express.Request, res: express.Response) => {
          const {params: {slug}} = req;
          try {
            Document.deleteParagraph(slug);
          } catch (e) {
            console.log(e);
            res.status(400).send();
            return;
          }

          res.status(200).send();
        });

sentenceRouter
    .route('/:idx')
    .post(
        param('slug').isString(),
        param('idx').isInt({min: 0}),
        body('sentence').isString(),
        (req: express.Request, res: express.Response) => {
          const {params: {slug, idx}, body: {sentence}} = req;
          const id = validator.toInt(idx);

          let p: Paragraph;

          try {
            p = Document.getParagraph(slug);
          } catch (e) {
            console.log(e);
            res.status(404).send();
            return;
          }

          try {
            p.addSentence(id, sentence);
          } catch (e) {
            console.log(e);
            res.status(400).send();
            return;
          }

          res.status(200).send();
        })
    .delete(
        param('slug').isString(),
        param('idx').isInt({min: 0}),
        (req: express.Request, res: express.Response) => {
          const {params: {slug, idx}} = req;
          const id = validator.toInt(idx);
          try {
            Document.getParagraph(slug).deleteSentence(id);
          } catch (e) {
            console.log(e);
            res.status(400).send();
            return;
          }

          res.status(200).send();
        });

