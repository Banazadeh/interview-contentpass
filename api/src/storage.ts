type Sentences = (string|null)[];

export class Paragraph {
  static numNonNull = (sentences: Sentences) => sentences
      .filter(((s) => s !== null))
      .length;

  constructor(numSentences: number, sentences?: Sentences) {
    if (numSentences < 0) {
      throw new Error('Invalid paragraph size.');
    }

    this.numSentences = numSentences;

    if (sentences) {
      this.sentences = sentences;

      // if all entries are not null the paragraph is complete
      if (Paragraph.numNonNull(sentences) === numSentences) {
        this.complete = true;
      }
    } else {
      // fill it all with null
      this.sentences = Array(numSentences).fill(null);
    }
  }
  complete: Boolean = false;
  numSentences: number = 0;
  sentences: Sentences = [];

  checkId(id: number) {
    if (id < 0 || id >= this.numSentences) {
      throw new Error('Invalid index for sentence.');
    }
  }

  updateComplete() {
    const numNonEmptySentences = Paragraph.numNonNull(this.sentences);
    if (
      !this.complete &&
      numNonEmptySentences === this.numSentences
    ) {
      this.complete = true;
    } else if (
      this.complete &&
      numNonEmptySentences < this.numSentences
    ) {
      this.complete = false;
    }
  }

  addSentence(id: number, sentence: string): void {
    this.checkId(id);
    // at the moment we override already created sentences
    // this can of course be changed
    this.sentences[id] = sentence;
    // update the complete flag if the paragraph is full
    this.updateComplete();
  }

  deleteSentence(id: number): void {
    this.checkId(id);
    this.sentences[id] = null;
    this.updateComplete();
  }

  clone(): Paragraph {
    return new Paragraph(this.numSentences, this.sentences);
  }
}

class Document {
  paragraphs: { [key: string ]: Paragraph } = {};

  slugInUse(slug: string): Boolean {
    return this.paragraphs[slug] instanceof Paragraph;
  }

  checkSlugExist(slug: string): void {
    if (!this.slugInUse(slug)) {
      throw new Error(`For the slug="${slug}" there exists no paragraph.`);
    }
  }

  getParagraph = (slug: string): Paragraph => {
    this.checkSlugExist(slug);
    return this.paragraphs[slug].clone();
  }

  getAllParagraphKeys = (): string[] => {
    return Object.keys(this.paragraphs);
  }

  createParagraph = (slug: string, numSentences: number): void => {
    this.paragraphs[slug] = new Paragraph(numSentences);
  }

  deleteParagraph = (slug: string): void => {
    this.checkSlugExist(slug);
    delete this.paragraphs[slug];
  }
}

export default new Document();
