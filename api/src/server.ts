'use strict';

import bodyParser from 'body-parser';
import express from 'express';
import {paragraphRouter} from './router';

const PORT = 8080;
const HOST = '0.0.0.0';

const app = express();


app.use(bodyParser.json());
app.use('/paragraph', paragraphRouter);
app.get('/', (_: express.Request, res: express.Response) => {
  res.send('Hello world\n');
});

app.listen(PORT, HOST, () => {
  console.log(`Server running on http://${HOST}:${PORT}`);
});
